class: center, middle

# Introduzione a Elm
### linguaggio funzionale per il browser
by naufraghi@develer.com

Code on: [https://bitbucket.com/naufraghi/guess](https://bitbucket.com/naufraghi/guess)

---
name: sintassi

# La sintassi

La sintassi di Elm assomiglia molto a quella di Haskell

---
template: sintassi

### Commenti
```haskell
-- a single line comment

{- a multiline comment
   {- can be nested -}
-}
```
Trucchetto:

```haskell
{--}
add x y = x + y
--}
```
Basta togliere / aggiungere un `}` per commentare / decommentare un blocco!

---
template: sintassi

### Condizionali

```haskell
if powerLevel > 9000 then "OVER 9000!!!" else "meh"
```

### Liste

```haskell
[1..4]
[1,2,3,4]
1 :: [2,3,4]
1 :: 2 :: 3 :: 4 :: []
```

---
template: sintassi

### Tipi

```haskell
type Specie = Umano | Cane
```

### Funzioni

```haskell
square n =
  n^2

hypotenuse a b =
  sqrt (square a + square b)
```

### Funzioni anonime:

```haskell
squares =
  List.map (\n -> n^2) [1..100]
```

---
template: sintassi

### Record

```haskell
point =                         -- create a record
  { x = 3, y = 4 }

.x point                        -- field access function

{ point |                       -- update fields
    x = 1,
    y = point.y + 1
}

dist {x,y} =                    -- pattern matching on fields
  sqrt (x^2 + y^2)

type alias Location =           -- type aliases for records
  { line : Int
  , column : Int
  }
```

---
template: sintassi

### Pattern Matching esaustivo
```haskell
import Html exposing (text)

type Specie = Umano | Cane

saluto animale =
  case animale of
    Umano -> "Ciao"
    Cane -> "Bau"

main =
  text (saluto Cane)
```

Provate ad aggiungere `Gatto` all'unione: [elm-lang.org/try](http://elm-lang.org/try) ed a compilare di nuovo.

--

Il resto della sintassi lo trovate spiegato qua: [Elm Syntax](http://www.elm-lang.org/docs/syntax)

---

# Elm Architecture

```haskell
import Html exposing (..)

-- MODEL
type alias Model = { ... }

-- UPDATE
type Msg = Reset | ...

update : Msg -> Model -> Model
update msg model =
  case msg of
    Reset -> ...
    ...

-- VIEW
view : Model -> Html Msg
view model =
  ...
```
Con questi 3 pezzi già si possono costruire applicazioni che girano nel browser, senza comunicare col resto del mondo, vedi l'esempio [Buttons](http://guide.elm-lang.org/architecture/user_input/buttons.html).

---

# Esercizi

- ## Implementate il tasto «reset» dell'esempio [Buttons](http://guide.elm-lang.org/architecture/user_input/buttons.html).

--

- ### Aggiungere la firma alla funzione `update` dell'esempio [Text Fields](http://guide.elm-lang.org/architecture/user_input/text_fields.html) semplice app che inverte un campo di testo.

```haskell
-- UPDATE
type Msg = NewContent String

-- update : ?? -> ?? -> ??
update (NewContent content) oldContent =
  content
```

---
# Esercizi

- ## [Forms](http://guide.elm-lang.org/architecture/user_input/forms.html) un esempio elementare di validazione password, fare gli esercizi proposti.

### [Documentazione Elm Core Libraries](http://package.elm-lang.org/packages/elm-lang/core/4.0.0)
