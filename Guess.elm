module Guess exposing (init, update, view)

import Html exposing (..)
import Html.Attributes exposing (style, class)
import Html.Events exposing (onClick)
import Http
import Json.Decode as Json exposing ((:=))
import Task
import String


-- MODEL

type alias Model =
    { gameid : Maybe Int
    , token : Maybe String
    , hint : String
    , result : Maybe Int
    , guesses : List Int
    }


init : ( Model, Cmd Msg )
init =
    ( { gameid = Nothing
      , token = Nothing
      , hint = "Guess a number"
      , result = Nothing
      , guesses = []
      }
    , startNewGame
    )


-- UPDATE

type alias GameStatus =
    { gameid : Int
    , hint : String
    , result : Maybe Int
    , guesses : List Int
    , token : Maybe String
    }


type Msg
    = StartGame
    | GuessNumber Int
    | GameStarted GameStatus
    | UnableToStartGame Http.Error
    | GameProgress GameStatus
    | UnableToProgressGame Http.Error


update : Msg -> Model -> ( Model, Cmd Msg )
update action model =
    case action of
        StartGame ->
            ( model, startNewGame )

        GameStarted gameStatus ->
            ( Model (Just gameStatus.gameid)
                gameStatus.token
                gameStatus.hint
                gameStatus.result
                gameStatus.guesses
            , Cmd.none
            )

        GuessNumber num ->
            ( model, guessNumber model num )

        GameProgress gameStatus ->
            ( { model
                | hint = gameStatus.hint
                , result = gameStatus.result
                , guesses = gameStatus.guesses
              }
            , Cmd.none
            )

        UnableToStartGame _ ->
            ( Model Nothing
                Nothing
                "Unable to start a new game"
                Nothing
                []
            , Cmd.none
            )

        UnableToProgressGame _ ->
            ( model, Cmd.none )


-- VIEW

view : Model -> Html Msg
view model =
-- view-helpers
    let
        targetClass num =
            let
                class status =
                    String.join " " [ "columns", status, "button" ]
            in
                case model.result of
                    Just res ->
                        class
                            ( if res == num then "success" else "disabled" )

                    Nothing ->
                        class "disabled"

        buttonAttrs num =
            if (List.member num model.guesses) then
                class (targetClass num)
            else
                class "columns button"

        guess num =
            div
                [ buttonAttrs num
                , onClick (GuessNumber num)
                ]
                [ text (" " ++ (toString num) ++ " ") ]

        guessRow row =
            div [ class "row" ] (List.map guess [(row * 10) + 1..(row + 1) * 10])

        guesses =
            List.map guessRow [0..9]

        message =
            case model.result of
                Nothing ->
                    "Guess a number between 1 and 100"

                Just _ ->
                    "Guessed!"
-- view-dsl
    in
        div []
            [ div [ class "title-bar " ]
                [ div [ class "title-bar-left" ]
                    [ span [ class "title-bar-title" ]
                        [ text "Guess Game in Elm + hug" ]
                    ]
                , div [ class "title-bar-right" ]
                    [ div [ class "button", onClick StartGame ]
                        [ text "New game" ]
                    ]
                ]
            , div
                [ class "row"
                , style [ ( "margin", "2em" ) ]
                ]
                [ div [ class "columns" ]
                    [ h1 [] [ text message ]
                    , h2 [] [ text model.hint ]
                    ]
                , div [ class "colums" ] guesses
                ]
            ]


-- TASKS

startNewGame : Cmd Msg
startNewGame =
    let
        play =
            Http.post decodeGame (Http.url "/play" []) Http.empty
    in
        Task.perform UnableToStartGame GameStarted play


decodeGame : Json.Decoder GameStatus
decodeGame =
    Json.object5 GameStatus
        ("gameid" := Json.int)
        ("hint" := Json.string)
        (Json.maybe ("result" := Json.int))
        ("guesses" := (Json.list Json.int))
        (Json.maybe ("auth" := Json.string))


guessNumber : Model -> Int -> Cmd Msg
guessNumber model guess =
    let
        token =
            case model.token of
                Just token ->
                    token

                Nothing ->
                    "<invalid>"

        gameid =
            case model.gameid of
                Just gameid ->
                    toString gameid

                Nothing ->
                    "<invalid>"

        url =
            "/game/" ++ gameid ++ "/guess/" ++ (toString guess)

        body =
            Http.string "{}"
    in
        Task.perform UnableToProgressGame GameProgress (authPUT token url body)


authPUT : String -> String -> Http.Body -> Task.Task Http.Error GameStatus
authPUT token url body =
    Http.send Http.defaultSettings
        { verb = "PUT"
        , headers =
            [ ( "Authorization", "Basic " ++ token )
            , ( "Content-Type", "application/json" )
            ]
        , url = url
        , body = body
        }
        |> Http.fromJson decodeGame
