import subprocess
import cog

code = ('%c' % 0x60) * 3
def put_code(source, lang):
    lang = lang or ''
    cog.outl('{code}{lang}'.format(code=code, lang=lang))
    cog.outl(source.strip())
    cog.outl('{code}'.format(code=code))


def flang(filename):
    if filename.endswith('py'):
        return 'python'
    elif filename.endswith('elm'):
        return 'haskell'
    elif filename.endswith('sh'):
        return 'bash'
    else:
        return ''


def take_file(filename, lang=None):
    lang = lang if lang is not None else flang(filename)
    source = open(filename).read()
    put_code(source, lang=lang)


def take_block(filename, block, lang=None):
    lang = lang if lang is not None else flang(filename)
    def _iter_block():
        in_block = False
        for line in open(filename).readlines():
            if line.strip().startswith('-- %s' % block):
                in_block = True
            elif in_block and line.strip().startswith('--'):
                in_block = False
            if in_block:
                yield line
    source = ''.join(_iter_block())
    put_code(source, lang=lang)


def take_expr(filename, expr, lang=None, blanks=2):
    lang = lang if lang is not None else flang(filename)
    def _iter_expr():
        in_expr = 0
        for line in open(filename).readlines():
            if line.strip().startswith('%s' % expr):
                in_expr = blanks
            elif in_expr > 0:
                if not line.strip():
                    in_expr -= 1
                else:
                    in_expr = blanks
            if in_expr > 0:
                yield line
    source = ''.join(_iter_expr())
    put_code(source, lang=lang)


def run(*args, lang=None):
    res = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()[0]
    pre = '$ %s' % " ".join(args)
    output = '\n'.join([pre, res.decode()])
    put_code(output, lang)
