#!/usr/bin/env python
# -*- coding: utf-8 -*-

import base64
import binascii
import json
import os
import random

import falcon  # for http status
import hug
from peewee import IntegerField, Model, SqliteDatabase, TextField, DoesNotExist

# Model

db = SqliteDatabase('games.db')


class IntListField(TextField):
    def db_value(self, value):
        return json.dumps(value)

    def python_value(self, value):
        return json.loads(value)


class Game(Model):
    target = IntegerField()
    guesses = IntListField()
    token = TextField()

    class Meta:
        database = db


db.connect()
db.drop_tables([Game], safe=True)
db.create_tables([Game], safe=True)


# Helper functions

def _game_state(game: Game) -> dict:
    hint = "try a number"
    result = None
    if game.guesses:
        last_guess = game.guesses[-1]
        if last_guess > game.target:
            hint = "try a smaller number"
        elif last_guess < game.target:
            hint = "try a bigger number"
        else:
            hint = "you have found the hidden number"
            result = game.target
    state = {'gameid': game.id,
             'hint': hint,
             'guesses': game.guesses}
    if result is not None:
        state['result'] = result
    return state


def _check_game_token(gameid: int, token: str):
    '''Helper function for `hug.authentication.basic`'''
    try:
        Game.get(Game.id == gameid, Game.token == token)
        return gameid
    except DoesNotExist:
        return False


# hug apis

@hug.get('/', output=hug.output_format.html)
def root():
    return open('index.html').read()


@hug.get('/elm.js', output=hug.output_format.html)
def elm():
    return open('elm.js').read()


@hug.post()
def play():
    '''
    Start a new game
    '''
    target = random.randint(1, 100)
    token = binascii.hexlify(os.urandom(5)).decode('utf8')
    game = Game(target=target, token=token, guesses=[])
    game.save()
    game_state = _game_state(game)
    auth = '{}:{}'.format(game.id, token).encode('utf8')
    game_state['auth'] = base64.b64encode(auth).decode('utf8')
    return game_state


@hug.put('/game/{gameid}/guess/{guess}', requires=hug.authentication.basic(_check_game_token))
def do_guess(gameid: hug.types.number, guess: hug.types.number, request, response):
    rgameid = int(request.context['user'])
    if rgameid != gameid:
        response.status = falcon.HTTP_409
        return {'errors': {'AuthError': 'Game / token mismatch'}}
    game = Game.get(Game.id == gameid)
    current_state = _game_state(game)
    if 'result' in current_state:
        response.status = falcon.HTTP_304
        return
    game.guesses.append(guess)
    game.save()
    return _game_state(game)
