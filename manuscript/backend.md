# Backend

## Backend Model

Il modello è minimale, l'unica cosa degna di nota è la definizione di un campo
custom per archiviare la lista di tentativi fatti `IntListField`.

```python
from peewee import (SqliteDatabase, Model, TextField, IntegerField)

db = SqliteDatabase('games.db')

class IntListField(TextField):
    def db_value(self, value):
        return json.dumps(value)

    def python_value(self, value):
        return json.loads(value)

class Game(Model):
    target = IntegerField()
    guesses = IntListField()
    token = TextField()

    class Meta:
        database = db
```

## Backend API

Le API con hug sono un piacere da scrivere, ed hanno una feature compresa nel
pacchetto: sono auto-documentanti. La documentazione viene generata a partire
dalle annotazioni di tipo, non è compatibile con *swagger* ma è molto
leggibile.

```python
@hug.post()
def play():
    '''
    Start a new game
    '''
    target = random.randint(1, 100)
    token = binascii.hexlify(os.urandom(5)).decode('utf8')
    game = Game(target=target, token=token, guesses=[])
    game.save()
    game_state = _game_state(game)
    auth = '{}:{}'.format(game.id, token).encode('utf8')
    game_state['auth'] = base64.b64encode(auth).decode('utf8')
    return game_state
```

Dove la funzione `_game_state(game)` non fa altro che convertire in JSON il
modello.

```python
def _game_state(game: Game) -> dict:
    hint = "try a number"
    result = -1
    if game.guesses:
        last_guess = game.guesses[-1]
        if last_guess > game.target:
            hint = "try a smaller number"
        elif last_guess < game.target:
            hint = "try a bigger number"
        else:
            hint = "you have found the hidden number"
            result = game.target
    return {'gameid': game.id,
            'hint': hint,
            'result': result,
            'guesses': game.guesses}
```

Se vi state chiedendo cosa è quel `b64encode` è solo una scorciatoia per usare
l'autenticazione `Basic Auth` già implementata in HUG.

A questo punto il nostro backend è già in grado di rispondere, per avviarlo
possiamo usare `hug` come runner:

```bash
$ hug -f app.py -p $C9_PORT
```

Ed adesso possiamo provare a fare una POST (con il comodissimo
[httpie](http://httpie.org/)):

```bash
(guess)naufraghi:~/workspace/guess (master) $ http POST $C9_HOSTNAME/play
```

![http POST /play](images/http-post.png)

Bene, adesso il server ha avviato una nuova partita ed assegnato un token
segreto con cui è possibile continuare la partita. Però dove stiamo andando
senza test? Per fortuna HUG ha una comoda interfaccia per simulare richieste
HTTP.

```python
@pytest.fixture(scope="module")
def new_game():
    random.seed(123)
    return hug.test.post(app, 'play')

def test_play(new_game):
    assert "auth" in new_game.data
```

In questo test controlliamo semplicemente di avere il campo `auth` nel valore
di ritorno della `POST`, abbiamo usato [py.test](http://pytest.org) per
scrivere i test e le sue comode fixture. Da notare come per rendere ripetibili
i test abbiamo bloccato il `seed` del modulo `random`, altrimenti ad ogni
esecuzione del test il numero da indovinare sarebbe diverso.

Ok, abbiamo un nuovo gioco con un token segreto, adesso dobbiamo implementare
l'end-point per provare ad indovinare il numero.

```python
@hug.put('/game/{gameid}/guess/{guess}')
def do_guess(gameid: hug.types.number,
             guess: hug.types.number,
             response):
    game = Game.get(Game.id == gameid)
    current_state = _game_state(game)
    if current_state['result'] != -1:
        response.status = falcon.HTTP_304
        return
    game.guesses.append(guess)
    game.save()
    return _game_state(game)
```

In questa prima implementazione non controlliamo il token, né gestiamo il caso
in cui il `gameid` non esiste, lo faremo tra poco.

Quello che facciamo è:

- recuperare il gioco corrente,
- controllare di non aver già trovato il numero
- aggiungere il nuovo tentativo alla lista

Grazie al `seed` che abbiamo impostato nella creazione del gioco possiamo
scrivere il test di una partita completa.

```python
def test_guess(new_game):
    def put(url):
        gameid = new_game.data['gameid']
        resp = hug.test.put(app, url.format(gameid=gameid))
        return resp
    resp = put('game/{gameid}/guess/50')
    assert resp.status == falcon.HTTP_200
    assert resp.data['hint'] == 'try a smaller number'
    resp = put('game/{gameid}/guess/6')
    assert resp.status == falcon.HTTP_200
    assert resp.data['hint'] == 'try a bigger number'
    resp = put('game/{gameid}/guess/7')
    assert resp.status == falcon.HTTP_200
    assert resp.data['hint'] == 'you have found the hidden number'
    resp = put('game/{gameid}/guess/7')
    assert resp.status == falcon.HTTP_304
```

Bene, a questo punto dobbiamo assicurarci che solo chi ha iniziato la partita
possa continuare a giorcare, è giunto il momento di cominciare ad usare il
nostro token.

HUG ha un buon supporto per i middleware, e ne integra alcuni basilari, come ad
esempio `Basic Auth`.

```python
@hug.put('/game/{gameid}/guess/{guess}',
         requires=hug.authentication.basic(_check_game_token))
def do_guess(gameid: hug.types.number,
             guess: hug.types.number,
             request, response):
    rgameid = int(request.context['user'])
    if rgameid != gameid:
        response.status = falcon.HTTP_409
        return {'errors': {'AuthError': 'Game / token mismatch'}}
    ...
```

Abbiamo passato una funzione di controllo dell'autenticazione tramite
l'argomento `requires` al decoratore `@hug.put`, specializzandola con una
nostra funzione `_check_game_token`.

```python
def _check_game_token(gameid: int, token: str):
    '''Helper function for `hug.authentication.basic`'''
    try:
        Game.get(Game.id == gameid, Game.token == token)
        return gameid
    except DoesNotExist:
        return False
```

Dai sorgenti di HUG possiamo vedere che l'utente del `Basic Auth` viene
iniettato nel `request.context` alla chiave `user`, nel nostro caso è proprio
il `gameid` che vogliamo essere coerente con quello dell'url.

A questo punto il backend del nostro gioco è completo, se vi interessano in
[test_app.py](https://bitbucket.org/naufraghi/guess/src/54d8814fab47baf0198408bcc7b61a1e6374f134/test_app.py?fileviewer=file-view-default#test_app.py-46)
ci sono alcuni test che non vi ho presentato necessari a portare al 100% il
coverage.

```bash
========================== test session starts ==========================
platform linux -- Python 3.5.1, pytest-2.8.5, py-1.4.31, pluggy-0.3.1
plugins: cov-2.2.0
collected 7 items

test_app.py .......
------------ coverage: platform linux, python 3.5.1-final-0 -------------
Name     Stmts   Miss  Cover
----------------------------
app.py      68      0   100%

======================= 7 passed in 0.97 seconds ========================
```
