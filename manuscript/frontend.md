# Frontend

<!-- [[[cog
from take import *
]]]
[[[end]]]
-->

## Architettura di Elm

A questo punto abbiamo un backend funzionante e testato, possiamo cominciare a
presentare il frontend. Per questa app useremo come base il modulo `Html.App`
che implementa la cosiddetta [Elm
Architecture](https://github.com/evancz/elm-architecture-tutorial#the-elm-architecture),
cioè la suddivisione dell'app in tre parti:

 1. `model` che rappresenta lo stato globale dell'app, la cui versione iniziale
    viene restituita dalla `init`,
 2. `update` che riceve l'input e lo stato corrente e restituisce il nuovo stato dell'app,
 3. `view` che dato lo stato genera l'output (l'HTML della nostra app).

Molto spesso quindi il modulo `Main` di una app scritta in *Elm* non farà altro
che includere i vari componenti, collegarli tra loro ed avviare la `Html.App`.

<!-- [[[cog take_file('Main.elm') ]]] -->
```haskell
module Main exposing (..)

import Guess exposing (..)
import Html.App as Html


main : Program Never
main =
    Html.program
        { init = init
        , update = update
        , view = view
        , subscriptions = \_ -> Sub.none
        }
```
<!-- [[[end]]] -->

Ci sono alcuni concetti che non sono certo chiari a prima vista, vediamone
alcuni.

- `Guess` è il nostro modulo, dal modulo prendiamo giusto le tre funzioni `init`,
  `update` e `view` che servono alla `Html.App`.
- `\_ -> Sub.none` è una funzione prendiposto dato che in questo esempio non
  abbiamo vere `subscriptions`, cioè non ci aspettiamo notifiche dal mondo
  di JavaScript, per ora non approfondiamo questo aspetto.

Per una introduzione più dettagliata vi consiglio la sezione
[Effects](http://guide.elm-lang.org/architecture/effects/index.html)
della Guida di *Elm* ed in particolare l'esempio HTTP
[Random Gif Viewer](http://guide.elm-lang.org/architecture/effects/http.html).

## Modello

Veniamo adesso al nostro modulo `Guess`, per cominciare abbiamo bisogno di un
modello, che deve contenere tutta l'informazione necessaria per la vista e per
fare la prossima mossa, nel nostro caso il modello sarà molto simile allo stato
del gioco restituito dal backend:

<!-- [[[cog take_block('Guess.elm', 'MODEL') ]]] -->
```haskell
-- MODEL

type alias Model =
    { gameid : Maybe Int
    , token : Maybe String
    , hint : String
    , result : Maybe Int
    , guesses : List Int
    }


init : ( Model, Cmd Msg )
init =
    ( { gameid = Nothing
      , token = Nothing
      , hint = "Guess a number"
      , result = Nothing
      , guesses = []
      }
    , startNewGame
    )
```
<!-- [[[end]]] -->

Già possiamo notare due cose:

 1. non esistono "variabili" che possono non avere un valore: il linguaggio
    offre dei costrutti di alto livello per definire la possibilità di non
    avere un valore. Lo stato iniziale del gioco è invalido, e quindi alcune
    variabili sono `Maybe <tipo>`. Dico "variabili" ma in realtà non
    possono variare, dato che tutto è immutabile.
 2. Tutto ha un tipo: stringhe, interi, liste di interi, non c'è spazio per
    errori a runtime per aver inserito una stringa in una lista di interi, il
    compilatore se ne accorge subito.

Abbiamo un tipo `Model` che è un particolare `record` con alcuni campi ben
definiti. La funzione `init` restituisce il valore iniziale e
contemporaneamente prepara un task pronto per essere eseguito nella
`startNewGame`.

Vediamo subito cosa fa la `startNewGame`:

<!-- [[[cog take_expr('Guess.elm', 'startNewGame') ]]] -->
```haskell
startNewGame : Cmd Msg
startNewGame =
    let
        play =
            Http.post decodeGame (Http.url "/play" []) Http.empty
    in
        Task.perform UnableToStartGame GameStarted play
```
<!-- [[[end]]] -->

`startNewGame` è una funzione che non prende argomenti e che restituisce un
`Cmd Msg` dove `Msg` è un tipo *somma* [^adt], nella terminologia di altri linguaggi
un *variant* o una *unione* disgiunta.

Tralasciando alcuni dettagli che vedremo tra poco, la `startNewGame` usa il
modulo `Http` per fare una `POST` sull'url `/play`, senza query `[ ]` e senza
body `Http.empty`, passa il risultato alla `decodeGame` che si occupa di
convertire il `json` in un record *Elm* o un errore. La `Task.perform` prepara
il task per il runtime, con un tipo da usare di errore `UnableToStartGame`,
ed un tipo da usare in caso di successo `GameStarted`.

Andiamo adesso a definire `Mag` come la combinazione in `or` di altri tipi.

<!-- [[[cog take_expr('Guess.elm', 'type Msg') ]]] -->
```haskell
type Msg
    = StartGame
    | GuessNumber Int
    | GameStarted GameStatus
    | UnableToStartGame Http.Error
    | GameProgress GameStatus
    | UnableToProgressGame Http.Error
```
<!-- [[[end]]] -->

Le opzioni di `Msg` vengono definite all'interno della stessa dichiarazione.
`StartGame` non ha altre informazioni, mentre gli altri tipi sono parametrizzati.
Vengono definiti tipi *prodotto*, in questo caso una coppia di tipi, dove lo spazio valido dei
"valori" è dato dal prodotto cartesiano dei tipi di ogni elemento della coppia.

Abbiamo detto che la `startNewGame` fa il lavoro sporco in questa app, paricamente tutte
le operazioni che possono fallire sono in questo ramo del codice.
Quella che ho presentato infatti è la versione semplice della storia, perché in realtà
sia la `post` che la decodifica del `json` potrebbero fallire.

La firma completa della `Http.post` è:

```haskell
post : Decoder value -> String -> Body -> Task Error value
```

cioè una funzione che prende 3 argomenti e restituisce un `Task` specializzato
su un errore di tipo `Http.Error` ed un `value` che è il tipo passato come
argomento alla `Decoder` JSON.

In realtà sto mentendo, perché in *Elm* come in altri linguaggi le funzioni sono
sempre applicate parzialmente, tutte le funzioni accettano al massimo un argomento
ed eventualmente restituiscono una nuova funzione che consumerà il prossimo.

Quindi `post` prende un `Decoder value` e restituisce `->` una nuova funzione che prende
una `String`, che a sua volta restituisce `->` una funzione che prende un `Body` che
infine restituisce `->` il `Task` finale.

Per tornare nella nostra applicazione usiamo la `Task.perform`:

```haskell
perform : (x -> msg) -> (a -> msg) -> Task x a -> Cmd msg
```

che invia al runtime in task da eseguire. L'argomento più importante è il
`Task` che descrive cosa si vuole fare. Ma è necessario anche fornire due
funzioni da usare come marcatori in caso di successo o fallimento, nel nostro
caso `UnableToStartGame` e `GameStarted`.


## Vista

Vediamo adesso come si compone una vista in *Elm*, per cominciare anche la
vista è una funzione, che prende come argomento il modello `Model` e
restituisce `Html Msg`, cioè le informazioni necessare a creare il vero e
proprio codice HTML della pagina ed i messaggi che questa pagina può emettere.

<!-- [[[cog take_block('Guess.elm', 'VIEW') ]]] -->
```haskell
-- VIEW

view : Model -> Html Msg
view model =
```
<!-- [[[end]]] -->

In *Elm* non è possibile inserire HTML direttamente nel codice, si usa invece
il modulo `Html` che espone una serie di funzioni mappate sui tag HTML.

<!-- [[[cog take_block('Guess.elm', 'view-dsl') ]]] -->
```haskell
-- view-dsl
    in
        div []
            [ div [ class "title-bar " ]
                [ div [ class "title-bar-left" ]
                    [ span [ class "title-bar-title" ]
                        [ text "Guess Game in Elm + hug" ]
                    ]
                , div [ class "title-bar-right" ]
                    [ div [ class "button", onClick StartGame ]
                        [ text "New game" ]
                    ]
                ]
            , div
                [ class "row"
                , style [ ( "margin", "2em" ) ]
                ]
                [ div [ class "columns" ]
                    [ h1 [] [ text message ]
                    , h2 [] [ text model.hint ]
                    ]
                , div [ class "colums" ] guesses
                ]
            ]
```
<!-- [[[end]]] -->

Può sembrare scomodo a prima vista dover imparare un nuovo linguaggio, ma in
reatà se ne apprezzano presto i vantaggi.

La sintassi base di un nodo html è `<tag> [lista di attributi] [lista di nodi figli]`, appena
imparate le basi non capita spesso di dover ricorrere alla documentazione perché quasi tutti
i nomi sono quelli già conosciamo dall'html.

A parte `div`, `span` ed altre funzioni abbastanza intuitive, possiamo notare
come si emette un messaggio dalla vista: `onClick StartGame`, ad un certo
evento del DOM agganciamo l'emissione di un messaggio.

Vediamo adesso la parte dinamica della vista, che mette ben in vista la potenzialità
di avere un DSL per la scrittura dell'HTML, con la possibilità di fattorizzare
la generazione del codice senza salti tra linguaggi diversi.

<!-- [[[cog take_block('Guess.elm', 'view-helpers') ]]] -->
```haskell
-- view-helpers
    let
        targetClass num =
            let
                class status =
                    String.join " " [ "columns", status, "button" ]
            in
                case model.result of
                    Just res ->
                        class
                            ( if res == num then "success" else "disabled" )

                    Nothing ->
                        class "disabled"

        buttonAttrs num =
            if (List.member num model.guesses) then
                class (targetClass num)
            else
                class "columns button"

        guess num =
            div
                [ buttonAttrs num
                , onClick (GuessNumber num)
                ]
                [ text (" " ++ (toString num) ++ " ") ]

        guessRow row =
            div [ class "row" ] (List.map guess [(row * 10) + 1..(row + 1) * 10])

        guesses =
            List.map guessRow [0..9]

        message =
            case model.result of
                Nothing ->
                    "Guess a number between 1 and 100"

                Just _ ->
                    "Guessed!"
```
<!-- [[[end]]] -->

Tra le varie funzioni di aiuto il tassello basilare è la funzione `guess`, che
rappresenta la singola cella del Guess Game.
Questa piccola funzione prende un numero come argomento ed aggancia all'evento
`onClick` l'emissione di un segnale che ha come valore `GuessNumber num`.
`guessRow` e `guesses` sono funzioni di aiuto per la generazione della griglia.

Vediamo ad esempio la funzione `guess`:

<!-- [[[cog take_expr('Guess.elm', 'guesses', blanks=1) ]]] -->
```haskell
guesses =
            List.map guessRow [0..9]
```
<!-- [[[end]]] -->

Restituisce una lista che è il risultato dell'applicazione di `guessRow 0`, `guessRow 1`, ...,
`guessRow 9`, che nel dettaglio:

<!-- [[[cog take_expr('Guess.elm', 'guessRow', blanks=1) ]]] -->
```haskell
guessRow row =
            div [ class "row" ] (List.map guess [(row * 10) + 1..(row + 1) * 10])
```
<!-- [[[end]]] -->

Restituisce un `<div class="row">` che a sua volta contiene un'altra lista di:

<!-- [[[cog take_expr('Guess.elm', 'guess num', blanks=1) ]]] -->
```haskell
guess num =
            div
                [ buttonAttrs num
                , onClick (GuessNumber num)
                ]
                [ text (" " ++ (toString num) ++ " ") ]
```
<!-- [[[end]]] -->

Che sono finalmente il nodo dell'interazione con l'utente che vi ho già presentato.

`buttonAttrs` e `targetClass` sono funzioni di aiuto che definiscono gli
attributi della cella.

Lo stile è standard se il `num` fornito non è stato ancora cliccato, nel caso invece
in cui sia stato "giocato", lo stile cambia se il numero è stato indovinato o meno.

Abbiamo visto che l'unico modo che ha la vista per comunicare col resto del mondo
è tramite l'emissione di messaggi. E che fine fanno questi messaggi? Vengono passati
al runtime di *Elm* che avvierà un ciclo di aggiornamento passandoli alla `update`.

Ogni aggiornamento non obbliga il browser a ridisegnare tutto, l'aggiornamento
della vista usa lo stesso algoritmo di aggiornamento della pagina di *React*,
vengono aggiornati solo i nodi modificati.


## Update

A questo punto l'app si è caricata, la vista ha ricevuto come input lo stato
iniziale del modello ed in modo asincrono ha avviato la richiesa per iniziare
una nuova partita. Appena la richiesa avrà un estito, sia un successo che un
fallimento, verrà invocata la funzione di `update`, che riceve in input un
`Msg`, il `Model` e restituisce la versione aggiornata del modello ed
eventuali «compiti per casa».

<!-- [[[cog take_expr('Guess.elm', 'update') ]]] -->
```haskell
update : Msg -> Model -> ( Model, Cmd Msg )
update action model =
    case action of
        StartGame ->
            ( model, startNewGame )

        GameStarted gameStatus ->
            ( Model (Just gameStatus.gameid)
                gameStatus.token
                gameStatus.hint
                gameStatus.result
                gameStatus.guesses
            , Cmd.none
            )

        GuessNumber num ->
            ( model, guessNumber model num )

        GameProgress gameStatus ->
            ( { model
                | hint = gameStatus.hint
                , result = gameStatus.result
                , guesses = gameStatus.guesses
              }
            , Cmd.none
            )

        UnableToStartGame _ ->
            ( Model Nothing
                Nothing
                "Unable to start a new game"
                Nothing
                []
            , Cmd.none
            )

        UnableToProgressGame _ ->
            ( model, Cmd.none )
```
<!-- [[[end]]] -->

Ecco come viene gestito l'esito della `startNewGame`, se arriva un nuovo stato,
possiamo aggiornare il modello, ed aspettare l'input utente, se invece lo stato
non arriva, ci mettiamo in una condizione di errore.

In caso dell'azione `GuessNumber num` il modello non viene modificato ma viene
emesso un effetto che avvia il dialogo col backend, la `guessNumber`, avrà come
esito una azione `GameProgress` oppure in caso di errore un `UnableToProgressGame`.
Anche qua in caso di errore non facciamo una cosa molto professionale e lasciamo tutto
fermo. In caso di successo aggiorniamo il modello con una sintassi
speciale dei record, che ha lo stesso effetto della `_replace` delle
`namedtuple` in Python: restituiamo una copia di `model` con alcuni campi aggiornati.

A questo punto la `Html.App` passa il modello aggiornato alla vista per
aggiornarla, ed il ciclo è chiuso.


[^adt]: [Algebraic data type](https://en.wikipedia.org/wiki/Algebraic_data_type)
