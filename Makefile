all: compile

compile: elm.js

ifeq ($(origin CI), undefined)
MAKEOPTS = --warn
else
MAKEOPTS = --yes
endif

elm.js: Main.elm Guess.elm
	elm make ${MAKEOPTS} Main.elm --output elm.js

test: compile
	py.test --cov app.py test_app.py

serve: compile
	xdg-open http://localhost:8000
	hug -f app.py

venv:
	pip3 install -r requirements.txt

npm:
	npm install -g elm

develop: venv npm

cog:
	cog.py -I . -r manuscript/*.md
