#!/usr/bin/env python
# -*- coding: utf-8 -*-


def demo(num: int, a: set):
    if num % 5:
        a.add(num)
    # some other code here.
    # ...
    # far far away.
    else:
        a.append(num * 2)
    return a


def strange(a: int, b: int) -> int:
    if a == b:
        return a + b
    # somehow a branch does not return
    # an int (no return in Python is
    # return None).


def run():
    demo(strange(1,2), [])


if __name__ == "__main__":
    run()
