# Hug + Elm = fun

Prendi una libreria strettamente Python 3, sì, hai letto bene, la "versione di
sviluppo" di Python [^1]:

![HUG: embrace the APIs of the future](https://camo.githubusercontent.com/ab5da64e2ce2f330820f29b95a932e3077a09eb6/68747470733a2f2f7261772e6769746875622e636f6d2f74696d6f74687963726f736c65792f6875672f646576656c6f702f617274776f726b2f6c6f676f2e706e67)

ed un linguaggio funzionale compilato verso JavaScript e cosa ottieni?
![Elm lang](https://pbs.twimg.com/profile_images/443551527307718656/cZHhsF-c.png)

Sicuramente una ventata di freschezza e tanto tanto type checking.

Python è un linguaggio agile, è facile prototipare, ma con un po' di pratica è
anche possibile creare progetti che sfidano gli anni [^2], scegliendo i propri
limiti invece di trovarseli imposti dal linguaggio.
Questa libertà ha però un prezzo, in particolare i refactoring sono sempre un
rischio perché il compilatore non fa praticamente niente più che trovare gli
errori di sintassi e tutto il resto avviene a runtime.
Per questo è obbligatorio avere una buona copertura di test ed è sempre buona
prassi "blindare" la parte di codice da modificare con ulteriori test prima di
affrontare qualsiasi modifica non banale.

Avendo giocato un po' con OCaml e Rust a volte mi manca l'aiuto del compilatore
quando scrivo in Python, spesso davvero l'errore che era sfuggito nel codice è
poco più che un errore di sintassi.

Ad esempio quante volte nello scrivere un algoritmo ho deciso che un `set` era
una struttura dati più adatta di una `list`? Tante, e quante le volte che nel
rinominare `data.append(x)` in `data.add(x)` me ne sono scordato uno in un
remoto `if` che ha dato errore solo grazie ai test? Tante, e quante invece
l'errore è saltato fuori solo usando il programma? Meno per fortuna, ma capita.

Ma non dovrebbe, nel 2016 mi aspetto di più da un linguaggio... ed infatti si
può avere di più!

Python dalla versione 3.0 accetta delle annotazioni ad argomenti e valori di
ritorno delle funzioni [^3], queste annotazioni sono libere ma in Python 3.5 è
stato introdotto il modulo `typing` e sono state standardizzate le annotazioni
di tipo. Ad oggi non esiste un type checker integrato in Python, ma il progetto
[mypy](http://www.mypy-lang.org) [^4], da cui è partito tutto, oggi vede come
ultimo committer il nostro Guido!

![Guido commits on mypy](https://i.imgur.com/3fiUwHg.png)

Questo mi da ottime speranze per il futuro, perché ad oggi *mypy* non mi è
stato molto utile, dato che solleva una valanga di errori nel controllare le
dipendenze del mio mini progetto.

Ma con un piccolo file di esempio invece l'errore che ci aspettiamo compare
subito:

```python
def demo(num: int, a: set):
    if num % 5:
        a.add(num)
    # some other code here
    # far far away
    else:
        a.append(num * 2)
    return a
```

```bash
$ mypy typedemo.py
typedemo.py: note: In function "demo":
typedemo.py:11: error: Set[Any] has no attribute "append"
```

Per chi preferisce una soluzione più integrata, una IDE come PyCharm già
integra un type checker ed in tempo record ha cominciato a supportare la
neonata sintassi:

![PyCharm typedemo.py](https://i.imgur.com/Ls0bW3F.png)

Quindi possiamo sperare che presto anche in Python tanti errori banali potranno
essere individuati a "check-time" invece che durante i test o peggio durante l'uso.

Ma in realtà Python in questo tipo di controlli è un novellino, c'è un
linguaggio che ha fatto degli errori di compilazione per essere umani il suo cavallo
di battaglia. [Elm](http://elm-lang.org) è un linguaggio funzionale che si compila
in JavaScript, il "traduttore" è scritto in Haskell. Non temete, di fatto non vi
capiterà spesso di doverlo compilare, perché viene distribuito come pacchetto
binario per `npm`.

Nato come libreria per scrivere giochi, si è evoluto negli ultimi anni come un
linguaggio completo per la scrittura di fontend complessi, senza niente da
invidiare a framework più famosi, dato che integra tutte le buzzword del momento:
*virtual-dom*, *functional reactive programming*, *time-travelling debugger*, e
la più importante *no runtime exceptions*.

Come è possibile non avere eccezioni a runtime? Per prima cosa non devono
esistere i tipi nullabili, niente più `null` o `None` su cui chiamare metodi o
da passare a funzioni che si aspettano un valore concreto, se una variabile può
non avere un valore dovremo definirla come `Maybe` oppure `Optional` in altri
linguaggi.

Un `Maybe <tipo a>` rappresenta una opzione tra `Just <valore di tipo a>` e
`Nothing`. Questo da solo non è molto diverso da come facciamo spesso in
python, cioè usare implicitamente `None` per codificare l'assenza di un
valore. Però dato che in python questa modifica è implicita, non sapremo
mai da una visione parziale del codice se il valore `None` è previsto
come caso possibile oppure no.

Quindi per completare il puzle è necessario un pattern matching esaustivo,
Vediamo come definire un `Maybe` in Elm e come il compilatore ci aiuta nel
gestire esaustivamente tutti i casi che potrebbero manifestarsi a runtime.

```haskell
> var = Just 123
Just 123 : Maybe.Maybe number
```

Abbiamo definito la variabile `var` come un `Just <numero>`.
`Just` non è altro che una delle due varianti del tipo `Maybe`, definito come:

```haskell
type Maybe a = Nothing | Just a
```

Grazie all'inferenza di tipi, anche se in questo caso non abbiamo definito il tipo
di `var`, questo viene dedotto dal letterale `123 : number` e quindi `var` assume il tipo
`var : Maybe number`, in particolare questa istanza ha valore `Just 123`.
```haskell
> case var of\
| Just num -> num
```
A questo punto per accedere al numero contenuto useremo il pattern matching, ma
in questo caso ci siamo scordati di gestire il caso `Nothing` in cui il valore
non è presente (l'esempio è minimale, ma immaginate l'accesso per chiave ad un
dizionario che può non contenere la chiave).
Questo codice è incompleto, `num` non è definito se `var` è `Nothing`, e fino
alla scorsa versione di Elm avremmo dovuto aspettare il momento dell'esecuzione
per scoprire che un ramo del match non era coperto, ma dall'ultima versione il
compilatore è diventato più "intelligente" e ci fa notare subito il problema.

```
============================== ERRORS ==============================

-- MISSING PATTERNS ------------------------------ repl-temp-000.elm

This `case` does not have branches for all possibilities.

4│>  case var of
5│>  Just num -> num

You need to account for the following values:

    Maybe.Nothing

Add a branch to cover this pattern!
```

Ma fa anche altro, se volete un assaggio vi consiglio di guardare il blog post
[Compilers as Assistants](http://elm-lang.org/blog/compilers-as-assistants)
dell'autore di Elm, [Evan Czaplicki](https://twitter.com/czaplic).

Con questi fondamenti (e se siete dei teorici dei linguaggi potrete elencare
gli altri requisiti) e con un passaggio controllato per accedere al mondo *usafe*
del JavaScript, abbiamo un linguaggio coerente per scrivere codice che girerà su
una VM JavaScript senza scrivere mai un rigo di JavaScript.

Lo so, JavaScript non è un brutto linguaggio, non peggio di altri, ma non ho
particolare interesse ad imparare un lunguaggio che assomiglia molto a Python
come linguaggio dinamico e amichevolmente anarchico. Però se voglio fare
qualcosa che sia compatibile col presente, *web 2.0*,
*se-non-c'è-la-web-app-non-esiste* ho l'ottima opportunità di usare JavaScript
solo come linguaggio target per un compilatore.

L'introduzione si è protratta fin troppo, passiamo all'implementazione.

Per questo esperimento mi sono preso la libertà di usare framework non mainstream:
- per le API ho scelto [HUG](https://github.com/timothycrosley/hug),
- a sua volta HUG usa [falcon](http://www.falconframework.org/), un framework
  WSGI minimale,
- invece come ORM ho scelto [peewee](http://docs.peewee-orm.com/).

Il backend della nostra app «Guess the number» ha 2 endpoint dinamici:

- `POST /play` per iniziare una nuova partita, con `id` univoco e `token`
  random associato,
- `PUT /game/{gameid}/guess/{guess}` per cercare di indovinare un numero.

Oltre a questi serve il file statici `index.html` e l'app JS, `elm.js`.

Potete usare un workspace cloud9 per fare le prove, clonando quello che ho
configurato nello scrivere questo post:
[ide.c9.io/naufraghi/guess](https://ide.c9.io/naufraghi/guess)

Breve libro work in progress [Elm per Pythonisti](https://leanpub.com/elm-per-pythonisti/).
