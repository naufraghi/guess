#!/usr/bin/env python
# -*- coding: utf-8 -*-

import base64
import random
import subprocess

import falcon
import hug
import pytest

import app


@pytest.fixture(scope="module")
def new_game():
    random.seed(123)
    return hug.test.post(app, 'play')

@pytest.fixture(scope="module")
def elm_make():
    subprocess.check_call(['make'])

def test_play(new_game):
    assert "auth" in new_game.data

def test_guess(new_game):
    def put(url):
        gameid = new_game.data['gameid']
        auth = new_game.data['auth']
        resp = hug.test.put(app, url.format(gameid=gameid),
                            headers={'Authorization': 'Basic %s' % auth})
        return resp
    resp = put('game/{gameid}/guess/50')
    assert resp.status == falcon.HTTP_200
    assert resp.data['hint'] == 'try a smaller number'
    resp = put('game/{gameid}/guess/6')
    assert resp.status == falcon.HTTP_200
    assert resp.data['hint'] == 'try a bigger number'
    resp = put('game/{gameid}/guess/7')
    assert resp.status == falcon.HTTP_200
    assert resp.data['hint'] == 'you have found the hidden number'
    resp = put('game/{gameid}/guess/7')
    assert resp.status == falcon.HTTP_304

def test_guess_no_auth(new_game):
    resp = hug.test.put(app, 'game/1/guess/50')
    assert resp.status == falcon.HTTP_401

def test_guess_no_game_url(new_game):
    gameid = new_game.data['gameid']
    auth = new_game.data['auth']
    resp = hug.test.put(app, 'game/{gameid}/guess/50'.format(gameid=999),
                        headers={'Authorization': 'Basic %s' % auth})
    assert resp.status == falcon.HTTP_409

def test_no_game_in_token():
    auth = '{}:{}'.format(999, 1234).encode('utf8')
    auth = base64.b64encode(auth).decode('utf8')
    resp = hug.test.put(app, 'game/{gameid}/guess/50'.format(gameid=999),
                        headers={'Authorization': 'Basic %s' % auth})
    assert resp.status == falcon.HTTP_401

def test_guess_wrong_game_token(new_game):
    gameid = new_game.data['gameid']
    auth = 'somerandomstuff'
    resp = hug.test.put(app, 'game/{gameid}/guess/50'.format(gameid=gameid),
                        headers={'Authorization': 'Basic %s' % auth})
    assert resp.status == falcon.HTTP_401


def test_static_routes(elm_make):
    for url in ('/', '/elm.js'):
        resp = hug.test.get(app, url)
        assert resp.status == falcon.HTTP_200
